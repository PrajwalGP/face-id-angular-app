import { RouterModule, Routes } from '@angular/router'
import { LoginComponent } from './components/login/login.component'
import { RegisterComponent } from './components/register/register.component'
import { HomeComponent } from './components/home/home.component'
import { CameraSnapshotComponent } from './components/camera-snapshot/camera-snapshot.component'

export const routes: Routes = [
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'capture',
    component: CameraSnapshotComponent
  },
  {
    path: '**',
    redirectTo: '/home'
  }
];

export const routing = RouterModule.forRoot(routes);
