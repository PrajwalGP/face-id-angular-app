import { Injectable, ViewContainerRef } from '@angular/core';
declare var jquery:any;
declare var $ :any;
declare const  faceapi;
const SSD_MOBILENETV1 = 'ssd_mobilenetv1'
const TINY_FACE_DETECTOR = 'tiny_face_detector'
const MTCNN = 'mtcnn'
let selectedFaceDetector = SSD_MOBILENETV1

// ssd_mobilenetv1 options
let minConfidence = 0.1

// tiny_face_detector options
let inputSize = 512
let scoreThreshold = 0.5

//mtcnn options
let minFaceSize = 20
let forwardTimes = []
let withFaceLandmarks = false
let withBoxes = true


@Injectable()
export class FaceDetectionService {

  constructor() { }

   getFaceDetectorOptions() {
    return selectedFaceDetector === SSD_MOBILENETV1
      ? new faceapi.SsdMobilenetv1Options({ minConfidence })
      : (
        selectedFaceDetector === TINY_FACE_DETECTOR
          ? new faceapi.TinyFaceDetectorOptions({ inputSize, scoreThreshold })
          : new faceapi.MtcnnOptions({ minFaceSize })
      )
  }

   getCurrentFaceDetectionNet() {
    if (selectedFaceDetector === SSD_MOBILENETV1) {
      return faceapi.nets.ssdMobilenetv1
    }
    if (selectedFaceDetector === TINY_FACE_DETECTOR) {
      return faceapi.nets.tinyFaceDetector
    }
    if (selectedFaceDetector === MTCNN) {
      return faceapi.nets.mtcnn
    }
  }

   isFaceDetectionModelLoaded() {
    return !!this.getCurrentFaceDetectionNet().params
  }

  //drawings
   resizeCanvasAndResults(dimensions, canvas, results) {
    const { width, height } = dimensions instanceof HTMLVideoElement
      ? faceapi.getMediaDimensions(dimensions)
      : dimensions
    canvas.width = width
    canvas.height = height

    // resize detections (and landmarks) in case displayed image is smaller than
    // original size
    return results.map(res => res.forSize(width, height))
  }

   drawDetections(dimensions, canvas, detections) {
     console.log('this',this)
    const resizedDetections = this.resizeCanvasAndResults(dimensions, canvas, detections)
    faceapi.drawDetection(canvas, resizedDetections)
  }

   drawLandmarks(dimensions, canvas, results, withBoxes = true) {
    const resizedResults = this.resizeCanvasAndResults(dimensions, canvas, results)

    if (withBoxes) {
      faceapi.drawDetection(canvas, resizedResults.map(det => det.detection))
    }

    const faceLandmarks = resizedResults.map(det => det.landmarks)
    const drawLandmarksOptions = {
      lineWidth: 2,
      drawLines: true,
      color: 'green'
    }
    faceapi.drawLandmarks(canvas, faceLandmarks, drawLandmarksOptions)
  }



   onChangeWithFaceLandmarks(e) {
    withFaceLandmarks = $(e.target).prop('checked')
  }

   onChangeHideBoundingBoxes(e) {
    withBoxes = !$(e.target).prop('checked')
  }

   updateTimeStats(timeInMs) {
    forwardTimes = [timeInMs].concat(forwardTimes).slice(0, 30)
    const avgTimeInMs = forwardTimes.reduce((total, t) => total + t) / forwardTimes.length
  }

  async  onPlay() {
    const self = this;
    const videoEl = $('video')[0]
    console.log('videoEl',videoEl)
    if(videoEl.paused || videoEl.ended || !this.isFaceDetectionModelLoaded())
      return setTimeout(() => this.onPlay())


    const options = this.getFaceDetectorOptions()

    const ts = Date.now()

    const faceDetectionTask = faceapi.detectSingleFace(videoEl, options)
    const result = withFaceLandmarks
      ? await faceDetectionTask.withFaceLandmarks()
      : await faceDetectionTask

    this.updateTimeStats(Date.now() - ts)

    const drawFunction = withFaceLandmarks
      ? (a,b,c,d)=>{this.drawLandmarks(a,b,c)}
      : (a,b,c,d)=>{this.drawDetections.bind(a,b,c,d)}
    console.log('face detected results',result)
    if (result) {
      drawFunction(videoEl, $('#overlay').get(0), [result], withBoxes)
    }
    setTimeout(() => this.onPlay())
  }


}
