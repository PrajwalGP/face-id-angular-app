import { Component, ViewChild, OnInit, Output, Input, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from "@angular/router";

import { BlockUIService } from '../../services/block-ui.service';
import { FaceDetectionService } from '../../services/face-detection-services';
import { UserRestService } from '../../services/user-rest.service';
import { AuthService } from '../../services/auth.service';

declare const  faceapi;
declare var $ :any;
declare var jquery:any;
const SSD_MOBILENETV1 = 'ssd_mobilenetv1'
const TINY_FACE_DETECTOR = 'tiny_face_detector'
const MTCNN = 'mtcnn'
let selectedFaceDetector = SSD_MOBILENETV1

// ssd_mobilenetv1 options
let minConfidence = 0.1
let scale = 0.80;

// tiny_face_detector options
let inputSize = 512
let scoreThreshold = 0.5

//mtcnn options
let minFaceSize = 20
let forwardTimes = []
let withFaceLandmarks = false
let withBoxes = true

@Component({
  selector: 'app-camera-snapshot',
  templateUrl: './camera-snapshot.component.html',
  styleUrls: ['./camera-snapshot.component.scss']
})
export class CameraSnapshotComponent implements OnInit {
  private mediaStream;
  private timeoutRef;
  private isCapturing;
  @Input() imageUrl: string;
  @Output() imageCreated = new EventEmitter();
  @ViewChild('video') private video;
  @ViewChild('canvas') private canvas;
  @ViewChild('canvas2') private canvas2;
  isCaptureDisabled: boolean = false;
  isFaceCapturing: boolean = false;
  type:string;
  loginData:any = {pin:''};
  errMsg:string;
  successMsg:string;
  registrationData:any = {};
  isPasswordVisible = false;

  constructor(
    private auth: AuthService,
    private route: ActivatedRoute,
    private ref: ChangeDetectorRef,
    private router: Router,
    private blockUI: BlockUIService,
    private userRestService: UserRestService,
    private faceDetectionService: FaceDetectionService
  ) {
    this.route.params.subscribe(params => {
      console.log('params',params)
                this.type = params["source"];
            });
}


  async ngOnInit() {
    await faceapi.loadTinyFaceDetectorModel('/weights')
    await faceapi.loadSsdMobilenetv1Model('/weights')
     // this.type = 'login';
    if(this.type == 'register'){
      this.registrationData  = localStorage.getItem('registrationData')
      typeof(this.registrationData) == 'string' ? this.registrationData = JSON.parse(this.registrationData): ''
      if((this.registrationData || {}).userName &&  (this.registrationData || {}).pin){
        this.getCameraImage();
      }
      else {
        this.router.navigate(['/register'])
      }
    }
    else{
      this.getCameraImage();
    }
  }

    togglePasswordView() {
      this.isPasswordVisible = !this.isPasswordVisible;
    }

     clearCanvas() {
      const ctx = this.canvas.nativeElement.getContext('2d');
      ctx.save();
      ctx.globalCompositeOperation = 'copy';
      ctx.strokeStyle = 'transparent';
      ctx.beginPath();
      ctx.lineTo(0, 0);
      ctx.stroke();
      ctx.restore();
    }

  getCameraImage() {
    $('#overlay-1')[0].hidden = false;
    // this.clearCanvas();
    // $('#overlay-2')[0].hidden = false;

    // block UI and reset image
    // this.blockUI.start();
    this.imageUrl = undefined;
    this.imageCreated.emit(undefined);

    // request access to camera for video
    navigator.getUserMedia(
      { video: true },
      async (mediaStream) => {
        this.mediaStream = mediaStream;
        this.video.nativeElement.src = window.URL.createObjectURL(mediaStream);
        try {
          // this.faceDetectionService.onPlay()
          this.onPlay();
        }
        catch (err) {
          console.log('err occured', err);
        }
        // setTimeout(() => {
        //   // create screenshot and emit as dataUrl
        //   var ctx = this.canvas.nativeElement.getContext('2d');
        //   ctx.drawImage(this.video.nativeElement, 0, 0, 500, 380);
        //   this.imageUrl = this.canvas.nativeElement.toDataURL()
        //   this.imageCreated.emit(this.imageUrl);

        //   //stop video and blockUI
        //   this.mediaStream.getVideoTracks()[0].stop();
        //   this.blockUI.stop();
        //   this.ref.detectChanges();
        // }, 3000);
      },
      error => { });
  }

  capture(){
    this.isCapturing = true;

    $('#overlay-1')[0].hidden = true;
    this.ref.detectChanges();
    clearTimeout(this.timeoutRef)
    this.isCapturing = false;
    var ctx = this.canvas2.nativeElement.getContext('2d');
    this.canvas2.width = this.video.nativeElement.videoWidth * scale;
    this.canvas2.height = this.video.nativeElement.videoHeight * scale;
    ctx.drawImage(this.video.nativeElement, 0, 0, this.canvas2.width, this.canvas2.height);
    this.imageUrl = this.canvas2.nativeElement.toDataURL('image/jpeg', 1)
    $('#overlay-2')[0].hidden = true
    // this.canvas.width = this.video.nativeElement.videoWidth * scale;
    // this.canvas.height = this.video.nativeElement.videoHeight * scale;
    // ctx.drawImage(this.video.nativeElement, 0, 0, this.canvas.width, this.canvas.height);
    // this.imageUrl = this.canvas.nativeElement.toDataURL('image/jpeg', 1)
    this.imageCreated.emit(this.imageUrl);

    //stop video and blockUI
    this.mediaStream.getVideoTracks()[0].stop();
    this.isCapturing = false;
    // this.blockUI.stop();
    if(this.type == 'login'){
    // this.login();
    this.router.navigate(['/home']);
  }
  else if(this.type == 'register'){
    this.registerFace();
  }
  }

  registerFace() {
    this.errMsg = '';
    this.successMsg = '';
    console.log('this.imageUrl', this.imageUrl);
    // post user data and image url
    this.userRestService.register({
      userName: this.registrationData.userName,
      pin: this.registrationData.pin,
      base64Photo: this.imageUrl.split('data:image/jpeg;base64,')[1]
    }).subscribe(data => {
      const token = data.token;
      this.auth.setToken(token);
      this.auth.setUser({ username: this.registrationData.username });
      localStorage.removeItem('registrationData');
      this.successMsg = 'Account creation success!';
      this.type = 'login'
    },(err)=>{
      console.log('err occured',err)
      this.errMsg = 'Failed to create an account, try again';
      this.getCameraImage();
    });
  }

  login(){
    this.errMsg = '';
    this.successMsg = '';
    this.userRestService.login({
      base64Photo: this.imageUrl.split('data:image/jpeg;base64,')[1],
      pin: this.loginData.pin
    })
      .subscribe(data => {
        // on success set token and user data
        var token = data.token;
        this.auth.setToken(token);
        this.auth.setUser({ username: data.message });
        // navigate to home page
        this.router.navigate(['/home']);
      }, (err)=>{
        console.log('error occured',err)
        this.errMsg = "Failed to login";
        this.getCameraImage();
      });
  }

  async  onPlay() {
    let withFaceLandmarks = false
    const self = this;
    const videoEl = $('video')[0]
    // if(videoEl.ended && this.timeoutRef){
    //   clearTimeout(this.timeoutRef)
    // }
    if (videoEl.paused || videoEl.ended || !this.faceDetectionService.isFaceDetectionModelLoaded()) {
      return (this.timeoutRef = setTimeout(() => this.onPlay()))
    }


    const options = this.faceDetectionService.getFaceDetectorOptions()

    const ts = Date.now()

    const faceDetectionTask = faceapi.detectSingleFace(videoEl, options)
    const result = withFaceLandmarks
      ? await faceDetectionTask.withFaceLandmarks()
      : await faceDetectionTask

    this.faceDetectionService.updateTimeStats(Date.now() - ts)

    const drawFunction = withFaceLandmarks
      ? this.drawLandmarks
      : this.drawDetections
    if (result) {
      this.isCaptureDisabled = false;
      if (withFaceLandmarks) {
        this.drawLandmarks(videoEl, $('#overlay-1')[0], [result], true)
      } else {
        this.drawDetections(videoEl, $('#overlay-1')[0], [result])
      }
    }
    else {
      this.isCaptureDisabled = true;
    }
    if (this.isCapturing != false) {
      this.timeoutRef = setTimeout(() => this.onPlay())
    }
  };
  getFaceDetectorOptions() {
    return selectedFaceDetector === SSD_MOBILENETV1
      ? new faceapi.SsdMobilenetv1Options({ minConfidence })
      : (
        selectedFaceDetector === TINY_FACE_DETECTOR
          ? new faceapi.TinyFaceDetectorOptions({ inputSize, scoreThreshold })
          : new faceapi.MtcnnOptions({ minFaceSize })
      )
  }

  getCurrentFaceDetectionNet() {
    if (selectedFaceDetector === SSD_MOBILENETV1) {
      return faceapi.nets.ssdMobilenetv1
    }
    if (selectedFaceDetector === TINY_FACE_DETECTOR) {
      return faceapi.nets.tinyFaceDetector
    }
    if (selectedFaceDetector === MTCNN) {
      return faceapi.nets.mtcnn
    }
  }

  isFaceDetectionModelLoaded() {
    return !!this.getCurrentFaceDetectionNet().params
  }

  //drawings
  resizeCanvasAndResults(dimensions, canvas, results) {
    const { width, height } = dimensions instanceof HTMLVideoElement
      ? faceapi.getMediaDimensions(dimensions)
      : dimensions
    canvas.width = width
    canvas.height = height

    // resize detections (and landmarks) in case displayed image is smaller than
    // original size
    return results.map(res => res.forSize(width, height))
  }

  drawDetections(dimensions, canvas, detections) {
    const resizedDetections = this.resizeCanvasAndResults(dimensions, canvas, detections)
    faceapi.drawDetection(canvas, resizedDetections)
  }

  drawLandmarks(dimensions, canvas, results, withBoxes = true) {
    const resizedResults = this.resizeCanvasAndResults(dimensions, canvas, results)

    if (withBoxes) {
      faceapi.drawDetection(canvas, resizedResults.map(det => det.detection))
    }

    const faceLandmarks = resizedResults.map(det => det.landmarks)
    const drawLandmarksOptions = {
      lineWidth: 2,
      drawLines: true,
      color: 'green'
    }
    faceapi.drawLandmarks(canvas, faceLandmarks, drawLandmarksOptions)
  }
  isLoginFormValid(){
    return (this.loginData.pin|| "").length == 4
  }
  isCaptureButtonsEnabled() {
    return this.type == 'register';
  }

  isLoginButtonsEnabled() {
    return this.type == 'login';
  }
  onChangeWithFaceLandmarks(e) {
    withFaceLandmarks = $(e.target).prop('checked')
  }

  onChangeHideBoundingBoxes(e) {
    withBoxes = !$(e.target).prop('checked')
  }

  updateTimeStats(timeInMs) {
    forwardTimes = [timeInMs].concat(forwardTimes).slice(0, 30)
    const avgTimeInMs = forwardTimes.reduce((total, t) => total + t) / forwardTimes.length
  }
  createAccount() {
    let registrationData = localStorage.getItem('registrationData')
  }

  routeToSignup() {
    this.ref.detectChanges();
    clearTimeout(this.timeoutRef)
    this.isCapturing = false;
    //stop video and blockUI
    this.mediaStream.getVideoTracks()[0].stop();
    this.router.navigate(['/register'])
  }
}
