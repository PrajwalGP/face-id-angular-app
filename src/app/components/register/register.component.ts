import { Component, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { UserRestService } from '../../services/user-rest.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  imageUrl;
  isPinVisible = false;
  isConfirmPinVisible = false;
  private user: any = {};
  registerMethod = 'camera';

  constructor(private userRestService: UserRestService, private router: Router, private auth: AuthService, private ref: ChangeDetectorRef) {
  }

  ngOnInit() { }
  isFormValid() {
    return ((this.user.username || '').length && ('' + this.user.pin || '').length == 4 && ('' + this.user.confirmPin || '').length == 4);
  }

  togglePinField() {
    this.isPinVisible = !this.isPinVisible;
  }

  toggleConfirmField() {
    this.isConfirmPinVisible = !this.isConfirmPinVisible;
  }

  register() {
    if (this.user.pin === this.user.confirmPin) {
      localStorage.setItem('registrationData', JSON.stringify({ userName: this.user.username, pin: this.user.pin }));
      this.router.navigate(['/capture', { source: 'register' }]);
    } else {
      Swal({
        title: 'PINs doesn\'t match',
        text: 'Pin and confirmPin should be same',
        type: 'warning'
      });
    }

  }


  // image changed handler for embedded components (image picker, camera snapshot)
  imageChanged(data) {
    this.imageUrl = data;
    this.ref.detectChanges();
  }

}
